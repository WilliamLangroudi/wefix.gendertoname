﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenderByName.Models
{
    public class GenderNameModel
    {
        public string Gender { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int Occurences { get; set; }
    }
}
