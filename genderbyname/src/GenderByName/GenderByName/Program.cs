﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using GenderByName.Models;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace GenderByName
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var path = ConfigurationManager.AppSettings["FilePath"];

            var file = Directory.GetFiles(path).FirstOrDefault();

            if (file == null)
                throw new FileNotFoundException();

            // Read File
            var streamReader = new StreamReader(file);
            var lines = File.ReadAllLines(file);

            var genderNames = new List<GenderNameModel>();

            var count = 0;
            foreach (var line in lines.Skip(1))
            {
                count++;
                
                var dataItem  = line.Split(';');

                var model = new GenderNameModel();
                
                model.Name = dataItem[0];

                int occurrence = 0;
                int.TryParse(dataItem[1], out occurrence);
                model.Occurences = occurrence;

                var genderGet = $"https://gender-api.com/get?name=" + model.Name;

                var client = new WebClient();

                var response = client.DownloadStringTaskAsync(genderGet).Result;

                if (count == 400)
                {
                    Thread.Sleep(60000);
                   
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();
                    Console.WriteLine("Sleeping for 1 minute. zzzz");
                    Console.WriteLine(stopwatch.ElapsedMilliseconds);

                    if (stopwatch.ElapsedMilliseconds == 60000)
                    {
                        stopwatch.Stop();
                    }

                }
                Console.WriteLine($"Response {count} sent {DateTime.Now}");

                
                var stuff = JObject.Parse(response);

                var gender = stuff["gender"].ToString();

                model.Gender = gender;

                genderNames.Add(model);
            }

            var resultsPath = ConfigurationManager.AppSettings["ResultsPath"];
            using (var w = new StreamWriter(resultsPath))
            {
                for (var i = 0; i < genderNames.Count; i++)
                {
                    var first = genderNames[i].Name;
                    var second = genderNames[i].Gender;
                    var third = genderNames[i].Occurences.ToString();
                    var line = string.Format("{0},{1},{2}", first, second, third);
                    w.WriteLine(line);
                    w.Flush();
                }
            }


            // foreach record -> request to api 


            // add response to list


            // write to new list

        }
    }
}
